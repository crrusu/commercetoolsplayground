import com.commercetools.api.client.ProjectApiRoot;
import com.commercetools.api.models.common.PagedQueryResponse;
import com.commercetools.api.models.customer.Customer;
import com.commercetools.api.models.customer.CustomerPagedQueryResponse;
import com.commercetools.api.models.product.Product;
import com.commercetools.api.models.product.ProductPagedQueryResponse;
import com.commercetools.api.models.project.Project;

import java.util.List;

public class CommerceToolsApiApplicaiton {
    public static void main(String[] args) {
        ProjectApiRoot apiRoot = Client.createApiClient();

        Project myProject = apiRoot.get().executeBlocking().getBody();

        System.out.println(myProject.getName());

        CustomerPagedQueryResponse query = apiRoot.customers().get().executeBlocking().getBody();

        List<Customer> customers = query.getResults().stream().limit(10).toList();

        for (int i = 0; i < customers.size(); i++) {
            System.out.println("Customer " + customers.get(i).getFirstName() + " " + customers.get(i).getLastName());
        }
    }
}
