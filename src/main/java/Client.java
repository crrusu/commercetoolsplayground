import com.commercetools.api.client.ProjectApiRoot;
import com.commercetools.api.defaultconfig.ApiRootBuilder;
import com.commercetools.api.defaultconfig.ServiceRegion;
import io.vrap.rmf.base.client.oauth2.ClientCredentials;

public class Client {
    public static ProjectApiRoot createApiClient() {
        final ProjectApiRoot apiRoot = ApiRootBuilder.of()
                .defaultClient(ClientCredentials.of()
                        .withClientId("n8v3jX9HThwZ11sdzIXSoBHG")
                        .withClientSecret("JDz-f-C016ENIi0FVhFQNg0rLpC_qhEH")
                        .build(), ServiceRegion.GCP_EUROPE_WEST1)
                .build("test-project-crrusu");

        return apiRoot;
    }
}
